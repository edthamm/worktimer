#! /usr/bin/env python

from datetime import datetime, timedelta
from typing import Optional


def calculate_end_of_work(
    start_of_work: Optional[datetime] = None,
    break_time: timedelta = timedelta(minutes=45),
) -> datetime:
    start_of_work = datetime.now()

    end_of_work = start_of_work + timedelta(hours=8) + break_time

    return end_of_work


if __name__ == "__main__":
    # TODO allow input of start of work time
    print(calculate_end_of_work())
